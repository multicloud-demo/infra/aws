terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "multicloud-demo"

    workspaces {
      name = "aws"
    }
  }
}